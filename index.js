const infopack = require('infopack');
const folderToInfopack = require('infopack/dist/lib/generators/folder-to-infopack');

let pipelineSteps = [
    // change/edit as you like
    folderToInfopack.step()
];

let pipeline = new infopack.default(pipelineSteps);

pipeline.run();
